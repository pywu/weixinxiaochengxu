/**
 * 调用和风天气、腾讯地图
 *
 * @author 小帅丶
 * @date 2019年9月
 */
let key = ''//和风天气key
let weatherUrl = 'https://devapi.qweather.com/v7/weather/now';//常规天气数据API 如果是付费用户请注意自行更换API 免费订阅每天免费1千次

let tencentKey = ''//腾讯地图key
let tencentUrl = 'https://apis.map.qq.com/ws/geocoder/v1/?key='+tencentKey+'&location='//逆地址解析API 个人每天免费1万次
//获取区县名称
let getLocationName = (longitude, latitude, callback)=>{
  let location = latitude +","+longitude;
  wx.request({
    url: tencentUrl+location,
    method:'GET',
    success: function (res) {
      callback.success(res.data)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//获取实况天气
let weatherRequest = (longitude, latitude, callback) => {
  //拼接接口动态URL参数
  let location = longitude + "," + latitude;
  //发送接口请求
  wx.request({
    url: weatherUrl + '?location=' + location + '&key=' + key,
    method: 'GET',
    success: function (res) {
      callback.success(res.data.now)
    },
    fail: function (res) {
      if (callback.fail)
        callback.fail()
    }
  })
}
//暴露出去的接口
module.exports = {
  weatherRequest: weatherRequest,
  getLocationName:getLocationName
}
