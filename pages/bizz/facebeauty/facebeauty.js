var app = getApp();
var api = require('../../../utils/api')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [], isIcon: {}, scrollTop: 0,
    img:'',
    resultData:null,
    faceNum:0,
    apiSource:'百度AI-人脸检测'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({
      list: [{icon: '_icon-home-o', num: 1}],
      isIcon: {down: 'cicon-unfold-less', top: 'cicon-eject', up: 'cicon-unfold-more'}
    })
  },
  tapToolsBar(e) {
    var pageNum = e.detail.item.num;
    if(pageNum==1){
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
},
// 监听用户滑动页面事件。
onPageScroll(e) {
    this.setData({
        scrollTop: e.scrollTop
    })
},
  //请求方法
  chooseImageFile: function () {
    var that = this;
    that.setData({
      img:'',
      resultData:null,
      faceNum:0,
    })
    wx.chooseMedia({
      count: 1, 
      mediaType:['image'],
      sizeType: ['compressed'], 
      sourceType: ['album', 'camera'], 
      success: function (res) {
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.generalDetect(res.tempFiles[0].tempFilePath);
        }
      },
    })
  },
  //从聊天页面选择图片
  chooseMessage:function(){
    var that = this;
    that.setData({
      img:null,
      resultData:null,
      faceNum:null,
    })
    wx.chooseMessageFile({
      count: 1,
      sizeType: ['compressed'],
      type:'image',
      success(res){
        if (res.tempFiles[0].size > (4096 * 1024)) {
          wx.showToast({
            title: '图片文件过大哦',
            icon: 'none',
            mask: true,
            duration: 1500
          })
        } else {
          that.setData({
            img: res.tempFiles[0].path
          })
          that.generalDetect(res.tempFiles[0].path);
        }
      }
    })
  },
    //通用识别
    generalDetect(file){
      var that = this;
      that.setData({
        img: file
      }),
      wx.showLoading({
        title: "颜值分析中...",
        mask: true
      }),
      api.generalRequest(file, app.globalData.userId,api.facedetect_url, {
        success(result) {
          var resultJ = JSON.parse(result)
          wx.hideLoading();
          if (resultJ.code == 200) {
            that.setData({
              resultData: resultJ.data.face_list,
              faceNum: resultJ.data.face_num,
              img: 'data:image/jpg;base64,' + resultJ.data.deal_image
            })
          } else {
            if (resultJ.code == 87014) {
              wx.hideLoading();
              wx.showModal({
                content: '存在敏感内容，请更换图片',
                showCancel: false,
                confirmText: '明白了'
              })
              that.setData({
                img: null
              })
            } else {
              wx.hideLoading();
              wx.showModal({
                content: resultJ.msg_zh,
                showCancel: false,
                confirmText: '明白了'
              })
            }
          }
        }
      })
    },
    /**
 * 点击查看图片，可以进行保存
 */
preview(e) {
  var that = this;
  wx.previewImage({
    urls: [that.data.img],
    current: that.data.img
  })
},
previewCut(e) {
  var that = this;
  wx.previewImage({
    urls: [e.target.dataset.img],
    current: e.target.dataset.img
  })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  }, 
})