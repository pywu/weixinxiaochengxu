const app = getApp();
var weatherApi = require('../../utils/weather.js');
var DateUtil = require('../../utils/util.js');
var api = require('../../utils/api.js');
Page({
  data: {
    scrollTop: 0,
    promptTime: '',
    promptDay: '',
    areaName:'未知',
    promptTimeSole: '',
    swiperList: [{
        img: "/static/img/index.png"
      },
      {
        img: "/static/img/45fc.png"
      },
      {
        img: "/static/img/d0f2.png"
      }
    ],
    navData: [{
      title: '趣玩AI',
      sub: [{
          icon: 'cicon-emoji-o',
          colorName: 'cyan',
          name: 'beauty',
          dir:'aiindex',
          jump:true,
          title: 'AI颜值'
        },
        {
          icon: 'cicon-paint',
          colorName: 'blue',
          dir:'aiindex',
          name: 'health',
          jump:false,
          title: 'AI健康'
        },
        {
          icon: 'cicon-topbar',
          colorName: 'red',
          name: 'recovery',
          dir:'aiindex',
          jump:false,
          title: 'AI恢复'
        },
        {
          icon: 'cicon-palette',
          colorName: 'orange',
          name: 'examine',
          dir:'aiindex',
          jump:false,
          title: 'AI审核'
        },
        {
          icon: 'cicon-Aa',
          colorName: 'olive',
          name: 'handle',
          dir:'aiindex',
          jump:false,
          title: 'AI处理'
        },
        {
          icon: 'cicon-magic',
          colorName: 'green',
          name: 'xtools',
          dir:'aiindex',
          jump:true,
          title: '小工具'
        },
        {
          icon: 'cicon-DarkMode',
          colorName: 'brown',
          name: 'chatbot',
          dir:'bizz',
          jump:false,
          title: 'AI闲聊'
        }
      ]
    }],
  },
  onLoad: function () {
    // 初始化天气预报
    this.getWeather();
    //初始化日期
    this.setData({
      promptTime: DateUtil.formatTime(new Date()),
      promptDay: DateUtil.formatDay(new Date()),
      promptTimeSole: DateUtil.formateSole()
    });
    // 获取刷新内容
    this.getCopyWrite();
    // 获取轮播图
    this.getRotary();
  },
  // 监听用户滑动页面事件。
  onPageScroll(e) {
    // 注意：请只在需要的时候才在 page 中定义此方法，不要定义空方法。以减少不必要的事件派发对渲染层-逻辑层通信的影响。
    // 注意：请避免在 onPageScroll 中过于频繁的执行 setData 等引起逻辑层-渲染层通信的操作。尤其是每次传输大量数据，会影响通信耗时。
    // https://developers.weixin.qq.com/miniprogram/dev/reference/api/Page.html#onPageScroll-Object-object
    this.setData({
      scrollTop: e.scrollTop
    })
  },
  //获取刷新内容
  getCopyWrite: function () {
    var that = this;
    wx.request({
      url: api.copywrite_url,
      success: function (res) {
        console.info(res)
        if (res.data.code == 200) {
          that.setData({
            aboveText: res.data.data.aboveText,
            belowText: res.data.data.belowText
          })
        }
      }
    })
  },
    //获取轮播图片列表
    getRotary:function(){
      var that = this;
      wx.request({
        url: api.rotary_url+'?cachetBanner=1',
        success:function(res){
          if (res.data.code==200){
            that.setData({
              swiperList: res.data.data
            })
          }
        },
        fail:function(res){
          that.setData({
            swiperList: [{
              id: 0,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84000.jpg'
            }, {
              id: 1,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big84001.jpg',
            }, {
              id: 2,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big39000.jpg'
            }, {
              id: 3,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big10001.jpg'
            }, {
              id: 4,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big25011.jpg'
            }, {
              id: 5,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big21016.jpg'
            }, {
              id: 6,
              type: 'image',
              imgUrl: 'https://ossweb-img.qq.com/images/lol/web201310/skin/big99008.jpg'
            }]
          })
        }
      })
    },  
  //获取经纬度并获取天气预报和所在区县
  getWeather: function () {
    var that = this;
    let weatherIconURL = "https://xai-1251091977.cos.ap-chengdu.myqcloud.com/weather/";
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
        var latitude = res.latitude
        var longitude = res.longitude
        weatherApi.getLocationName(longitude,latitude,{
          success(res){
            that.setData({
              areaName:res.result.address_component.district
            })
          }
        })
        weatherApi.weatherRequest(longitude, latitude, {
          success(res) {
            that.setData({
              weather: res,
              weatherIcon: weatherIconURL + res.icon + 'd.png'
            })
          }
        });
      },
    })
  }
})
