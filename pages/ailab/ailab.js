const app = getApp();
var api = require('../../utils/api.js');
Page({
    /**
     * 页面的初始数据
     */
    data: {
        gridEasyDLList: [
            {
                icon: '../../static/images/ailife/chinesehm.png',
                color: 'red',
                badge: 20,
                id: 11,
                name: '中草药材'
            }, {
                icon: '../../static/images/ailife/watch.png',
                color: 'red',
                badge: 20,
                id: 12,
                name: '小表帝(手表识别)'
            }, {
                icon: '../../static/images/ailife/schoolbadge.png',
                color: 'red',
                badge: 20,
                id: 13,
                name: '高校校徽'
            }, {
                icon: '../../static/images/ailife/disease.png',
                color: 'red',
                badge: 14,
                id: 14,
                name: '农业病害'
            }]
    },
    //页面跳转
    toPage: function (event) {
        var route = event.currentTarget.id;
        if (route == 11) {
            wx.navigateToMiniProgram({
                appId: 'wxa68d018535a3f2ae'
            })
        }
        if (route == 12) {
            wx.navigateTo({
                url: '/pages/bizz/watch/watch',
            })
        }
        if (route == 13) {
            wx.navigateTo({
                url: '/pages/bizz/schoolbadge/schoolbadge',
            })
        }
        if (route == 14) {
            wx.navigateTo({
                url: '/pages/bizz/disease/disease',
            })
        }

    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow() {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide() {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload() {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom() {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage() {

    }
})
