import {
  colorUI
} from './config/ColorUI'
import {
  colorUISdk
} from './config/mp-sdk'
var api = require('utils/api.js');
App({
  colorUI,
  colorUISdk,
  onLaunch() {
    var that = this;
    that.xsShare()
    wx.getStorage({
      key: 'userLogin',
      success: function(res) {
        that.globalData.openid = res.data.openid,
        that.globalData.account_code = api.account_code
        that.globalData.userId = res.data.userId
      },
      fail:function(res){
        // 登录
        wx.login({
          success: res => {
            //发送 res.code 到后台换取 openId, sessionKey, unionId
            wx.request({
              url: api.wxlogin_url,
              data: {
                code: res.code,
                account_code: api.account_code
              },
              success: function (res) {
                that.globalData.openid = res.data.data.openid,
                that.globalData.account_code = api.account_code
                that.globalData.userId = res.data.data.userId
                wx.setStorage({
                  key: 'userLogin',
                  data: res.data.data,
                })
              },
            })
          }
        })
      }
    })
  },
  onShow() {

  },
  xsShare: function () {
    wx.onAppRoute(function () {
      let pages = getCurrentPages(),
        views = pages[pages.length - 1],
        data;
      if (views) {
        data = views.data;
        if (!data.isOverShare) {
          data.isOverShare = true;
          views.onShareAppMessage = function () {
            return {
              title: '有趣～ 好玩～ AI科技狠活',
              path: '/pages/index/index'
            }
          }
        }
      }
    })
  },
  globalData: {
    canIUse:false,
    openid: null,
    userId: null,
    account_code: null,
    session_key:null,
    userInfo:null,
  }
})
